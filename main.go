package main

import (
	"fmt"
	"os"
	"runtime"
	"time"

	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
)

const (
	windowWidth  = 800 // width of window
	windowHeight = 600 // height of window
)

func main() {
	if err := run(); err != nil {
		fmt.Fprintf(os.Stderr, "could not initialize SDL: %v", err)
		os.Exit(1)
	}
}

func run() error {
	err := sdl.Init(sdl.INIT_EVERYTHING)
	if err != nil {
		return fmt.Errorf("could not initialize SDL: %v", err)

	}
	defer sdl.Quit()

	if err := ttf.Init(); err != nil {
		return fmt.Errorf("could not initialize TTF: %v", err)
	}
	defer ttf.Quit()

	window, render, err := sdl.CreateWindowAndRenderer(windowWidth, windowHeight, sdl.WINDOW_SHOWN)
	if err != nil {
		return fmt.Errorf("could not create window: %v", err)
	}
	defer window.Destroy()

	if err := drawTilte(render, "Flappy Bird"); err != nil {
		return fmt.Errorf("could not draw Title: %v", err)
	}

	time.Sleep(time.Second * 3)

	scene, err := newScene(render)
	if err != nil {
		return fmt.Errorf("could not create scence: %v", err)
	}
	defer scene.destroy()

	events := make(chan sdl.Event)
	errc := scene.run(events, render)

	runtime.LockOSThread()
	for {
		select {
		case events <- sdl.WaitEvent():
		case err := <-errc:
			return err
		}
	}
	return nil
}

func drawTilte(r *sdl.Renderer, text string) error {
	r.Clear()

	f, err := ttf.OpenFont("res/fonts/Flappy.ttf", 20)
	if err != nil {
		return fmt.Errorf("could not load font: %v", err)
	}
	defer f.Close()

	c := sdl.Color{R: 255, G: 100, B: 0, A: 255}
	surface, err := f.RenderUTF8Solid(text, c)
	if err != nil {
		return fmt.Errorf("could not render tilte: %v", err)
	}
	defer surface.Free()

	t, err := r.CreateTextureFromSurface(surface)
	if err != nil {
		return fmt.Errorf("could not create textutre: %v", err)
	}
	defer t.Destroy()

	// titleLoc := &sdl.Rect{X:200, Y:200, W:100, H:100}
	if err := r.Copy(t, nil, nil); err != nil {
		return fmt.Errorf("could not copy texture: %v", err)
	}

	r.Present()

	return nil
}
