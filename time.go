package main

import "time"

type timer struct {
	time time.Time
}

func newTimer() *timer {
	return &timer{time.Now()}
}

func (t *timer) stop() int {
	start := t.time
	end := time.Now()
	return int(end.Sub(start).Seconds())
}

func (t *timer) restart() {
	t.time = time.Now()
}
