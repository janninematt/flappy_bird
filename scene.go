package main

import (
	"fmt"
	"log"
	"time"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
)

type scene struct {
	bg    *sdl.Texture
	bird  *bird
	pipes *pipes
	time  *timer
}

func (s *scene) run(events <-chan sdl.Event, r *sdl.Renderer) <-chan error {
	errc := make(chan error)
	go func() {
		defer close(errc)
		ticker := time.Tick(70 * time.Millisecond)
		levelUpTicker := time.Tick(3 * time.Second)
		done := false
		for !done {
			select {
			case event := <-events:
				if done = s.handleEvent(event); done {
					return
				}
			case <-ticker:
				s.update()

				if s.bird.isDead() {
					drawTilte(r, fmt.Sprintf("Game Over    %d    ", s.time.stop()))
					time.Sleep(3 * time.Second)
					s.restart()
				}

				if err := s.paint(r); err != nil {
					errc <- fmt.Errorf("cound not paint scence: %v", err)
				}
			case <-levelUpTicker:
				s.levleUp()
			}
		}
	}()
	return errc
}

func (s *scene) handleEvent(event sdl.Event) bool {
	switch event.(type) {
	case *sdl.QuitEvent:
		return true
	case *sdl.KeyboardEvent, *sdl.MouseButtonEvent:
		s.bird.jump()
		return false
	default:
		log.Printf("unknow event: %T", event)
		return false
	}
}
func newScene(r *sdl.Renderer) (*scene, error) {
	bg, err := img.LoadTexture(r, "res/imgs/background.png")
	if err != nil {
		return nil, fmt.Errorf("could not load background image: %v", err)
	}

	bird, err := newBird(r)
	if err != nil {
		return nil, err
	}

	p, err := newPipes(r)
	if err != nil {
		return nil, err
	}

	return &scene{bg: bg, bird: bird, pipes: p, time: newTimer()}, nil
}

func (s *scene) paint(r *sdl.Renderer) error {
	r.Clear()

	if err := r.Copy(s.bg, nil, nil); err != nil {
		return fmt.Errorf("could not copy background: %v", err)
	}

	if err := s.bird.paint(r); err != nil {
		return err
	}

	if err := s.pipes.paint(r); err != nil {
		return err
	}

	r.Present()

	return nil
}

func (s *scene) destroy() {
	s.bg.Destroy()
	s.bird.destroy()
	s.pipes.destroy()
}

func (s *scene) update() {
	s.bird.update()
	s.pipes.update()
	s.pipes.touch(s.bird)
}

func (s *scene) restart() {
	s.bird.restart()
	s.pipes.restart()
	s.time.restart()
}

func (s *scene) levleUp() {
	s.bird.speedUp()
	s.pipes.speedUp()
}
