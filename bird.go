package main

import (
	"fmt"
	"sync"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
)

const (
	gravity   = 2
	jumpSpeed = 10
	initSpeed = 1
)

type bird struct {
	sync.RWMutex
	time     int
	textures []*sdl.Texture
	x, y     int32
	w, h     int32
	speed    float64
	dead     bool
}

func newBird(r *sdl.Renderer) (*bird, error) {
	var textures []*sdl.Texture
	for i := 1; i <= 4; i++ {
		path := fmt.Sprintf("res/imgs/bird_frame_%d.png", i)
		texture, err := img.LoadTexture(r, path)
		if err != nil {
			return nil, fmt.Errorf("could not load background image: %v", err)
		}
		textures = append(textures, texture)
	}
	return &bird{textures: textures, x: windowWidth / 2, y: 300, h: 50, w: 43, speed: 1, dead: false}, nil
}

func (b *bird) update() {
	b.Lock()
	defer b.Unlock()

	b.time++
	b.y -= int32(b.speed)
	if b.y < 0 {
		b.speed = -b.speed
		b.y = 0
		b.dead = true
	}
	b.speed += gravity
}

func (b *bird) paint(r *sdl.Renderer) error {
	b.RLock()
	defer b.RUnlock()

	birdLoc := &sdl.Rect{
		X: b.x, Y: (windowHeight - b.y) - b.h/2, W: b.w, H: b.h}
	birdIndex := b.time % len(b.textures)
	if err := r.Copy(b.textures[birdIndex], nil, birdLoc); err != nil {
		return fmt.Errorf("could not copy background: %v", err)
	}

	return nil
}

func (b *bird) jump() {
	b.Lock()
	defer b.Unlock()

	b.speed = -jumpSpeed
}

func (b *bird) destroy() {
	b.Lock()
	defer b.Unlock()

	for _, t := range b.textures {
		t.Destroy()
	}
}

func (b *bird) isDead() bool {
	b.Lock()
	defer b.Unlock()

	return b.dead
}

func (b *bird) restart() {
	b.Lock()
	defer b.Unlock()

	b.x = windowWidth / 2
	b.y = windowHeight / 2
	b.speed = initSpeed
	b.dead = false
}

func (b *bird) touch(p *pipe) {
	b.RLock()
	defer b.RUnlock()

	if p.x > b.x+b.w { // too far right
		return
	}

	if p.x+p.w < b.x { // too far left
		return
	}

	if !p.reverted && p.h < b.y-b.h/2 { // pipe is too low
		return
	}

	if p.reverted && (windowHeight-p.h) > b.y+b.h/2 { // pipe is too  high
		return
	}

	b.dead = true
}

func (b *bird) speedUp() {
	b.Lock()
	defer b.Unlock()
	b.speed++
}
