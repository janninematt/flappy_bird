# Flappy Bird

Flappy Bird is a clone of the famous Flappy Bird game developed in Go with bindings for SDL2.

follow the [flappy-gopher](https://github.com/campoy/flappy-gopher)


## Installation


dependency lib

- sdl2

    ```
    eopkg install sdl2-{,-image,-mixer,-ttf,-gfx}-devel
    ```
- libx11-dev or libx11-devel

    ```
    eopkg install libx11-devel
    ```

    if not install it, go compile sdl2 package will terminate by below error

    ```
    $ go get -v github.com/veandco/go-sdl2/{sdl,img,mix,ttf}
    github.com/veandco/go-sdl2 (download)
    github.com/veandco/go-sdl2/sdl
    # github.com/veandco/go-sdl2/sdl
    In file included from go_workspace/src/github.com/veandco/go-sdl2/sdl/mouse.go:9:0:
    /usr/include/SDL2/SDL_syswm.h:72:10: fatal error: X11/Xlib.h: No such file or directory
    #include <X11/Xlib.h>
            ^~~~~~~~~~~~
    compilation terminated.
    ```


go lib

- [go-sdl2](https://godoc.org/github.com/veandco/go-sdl2/sdl)

    ```
    go get -v github.com/veandco/go-sdl2/{sdl,img,mix,ttf}
    ```


## Images, fonts, and licenses

All the images used in this game are CC0 and obtained from [Clipart](https://openclipart.org/tags/flapping).
You can find atlernative birds in there, so you can mod the game!

The fonts are copied from https://github.com/deano2390/OpenFlappyBird.