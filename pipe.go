package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
)

type pipes struct {
	sync.RWMutex

	texture *sdl.Texture
	speed   int32

	pipes []*pipe
}

func (ps *pipes) update() {
	ps.Lock()
	defer ps.Unlock()

	var rem []*pipe
	for _, p := range ps.pipes {
		p.Lock()
		p.x -= ps.speed
		p.Unlock()
		if p.x+p.w > 0 {
			rem = append(rem, p)
		}
	}
	ps.pipes = rem
}

func (ps *pipes) paint(r *sdl.Renderer) error {
	ps.RLock()
	defer ps.RUnlock()

	for _, p := range ps.pipes {
		if err := p.paint(r, ps.texture); err != nil {
			return fmt.Errorf("could not paint pipe: %v", err)
		}
	}
	return nil
}

func (ps *pipes) destroy() {
	ps.Lock()
	defer ps.Unlock()

	ps.texture.Destroy()

}

func (ps *pipes) restart() {
	ps.Lock()
	defer ps.Unlock()

	ps.pipes = nil
	ps.speed = 5
}

func (ps *pipes) touch(b *bird) {
	ps.RLock()
	defer ps.RUnlock()

	for _, p := range ps.pipes {
		p.touch(b)
	}
}

func (ps *pipes) speedUp() {
	ps.Lock()
	defer ps.Unlock()
	ps.speed++
}

func newPipes(r *sdl.Renderer) (*pipes, error) {
	texture, err := img.LoadTexture(r, "res/imgs/pipe.png")
	if err != nil {
		return nil, fmt.Errorf("could not load pipe image: %v", err)
	}

	ps := &pipes{
		texture: texture,
		speed:   5,
	}

	go func() {
		for {
			ps.Lock()
			ps.pipes = append(ps.pipes, newPipe())
			ps.Unlock()
			time.Sleep(time.Second * 2)
		}
	}()
	return ps, nil
}

type pipe struct {
	sync.RWMutex

	x        int32
	w        int32
	h        int32
	reverted bool
}

func newPipe() *pipe {
	return &pipe{
		x:        windowWidth,
		h:        100 + int32(rand.Intn(150)),
		w:        50,
		reverted: rand.Float32() > 0.5,
	}
}

func (p *pipe) paint(r *sdl.Renderer, texture *sdl.Texture) error {
	p.RLock()
	defer p.RUnlock()

	rect := &sdl.Rect{X: p.x, Y: windowHeight - p.h, W: p.w, H: p.h}
	flip := sdl.FLIP_NONE
	if p.reverted {
		rect.Y = 0
		flip = sdl.FLIP_VERTICAL
	}

	if err := r.CopyEx(texture, nil, rect, 0, nil, flip); err != nil {
		return fmt.Errorf("could not copy pipe: %v", err)
	}

	return nil
}

func (p *pipe) touch(b *bird) {
	p.RLock()
	defer p.RUnlock()
	b.touch(p)
}
